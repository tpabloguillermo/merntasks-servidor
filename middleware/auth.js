const jwt = require("jsonwebtoken");

module.exports = function(req,res,next) {
   // leer token del header
   const token = req.header("x-auth-token");

   // ver si no hay token
   if(!token) {
      return res.status(401).json({ msg: "No hay token, permiso no válido" });
   }

   // validar token con el token y la palabra secreta del archivo .env
   try {
      const cifrado = jwt.verify(token,process.env.SECRETA);
      /*
         creo un campo en el REQ que se llama usuario y en este caso que es
         usuario: {
            id: usuario.id
         }
         va a guardar el objeto de usuario con el campo id :D 
      */
      req.usuario = cifrado.usuario;
      next();
   } catch (error) {
      return res.status(401).json({ msg: "Token no válido" });
   }
}