const Usuario = require("../models/Usuario");
const bcryptjs = require("bcryptjs");
const { validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");


exports.autenticarUsuario = async (req, res) => {
   // revisamos si los campos son correctos (pw, email y nombre)
   const errors = validationResult(req);
   if (!errors.isEmpty()) {
      return res.status(400).json({ errores: errors.array() });
   }

   // extraer email y pw del request
   const {email, password} = req.body;

   try {
      // revisar si está registrado
      let user = await Usuario.findOne({email});
      if(!user) {
         return res.status(400).json({msg: "El usuario no existe"});
      }

      // verificar pw
      /*
         compara el pw del request con el pw que está almacenado en la BD que este se trae cuando busco
         el usuario por email para ver si existe
      */
      const pwCorrecto = await bcryptjs.compare(password,user.password);
      if(!pwCorrecto){
         return res.status(400).json({msg: "Contraseña incorrecta"});
      }

      // si todo es correcto crear y firmar el JWT
      // se van a almacenar datos del usuario 
      const payload = {
         usuario: {
            id: user.id
         }
      };

      // firmar el token 
      /* 
         1° payload 
         2° palabra secreta 
         3° configuracion 
         4° el callback --> se pasa (error,token)
      */
      jwt.sign(payload,process.env.SECRETA, {
         expiresIn: 36000
      }, (error, token) => {
         if(error) throw error;

         // confirmation message
         res.json({ token });

      });
      
   } catch (error) {
      console.log(error);
   }
};

// obtiene el usuario autenticado
exports.usuarioAutenticado = async(req,res) => {
   try {
      const usuario = await Usuario.findById(req.usuario.id);
      res.json({usuario});
   } catch (error) {
      console.log(error);
      res.status(500).json({msg: "Hubo un error"});
   }
}