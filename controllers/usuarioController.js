const Usuario = require("../models/Usuario");
const bcryptjs = require("bcryptjs");
const { validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");


exports.crearUsuario = async (req, res) => {

   // revisamos si los campos son correcots (pw, email y nombre)
   const errors = validationResult(req);
   if (!errors.isEmpty()) {
      return res.status(400).json({ errores: errors.array() });
   }

   // extract email and password
   const { email, password } = req.body;

   try {
      // create new user
      let usuario = await Usuario.findOne({ email });

      if (usuario) {
         return res.status(400).json({ msg: "El usuario ya existe" });
      }

      // create new user    
      usuario = new Usuario(req.body);


      // hashing password
      const salt = await bcryptjs.genSalt(10);
      usuario.password = await bcryptjs.hash(password, salt);

      // save new user
      await usuario.save();

      // crear y firmar el JWT
      // se van a almacenar datos del usuario 
      const payload = {
         usuario: {
            id: usuario.id
         }
      };

      // firmar el token 
      /* 
         1° payload 
         2° palabra secreta 
         3° configuracion 
         4° el callback --> se pasa (error,token)
      */
      jwt.sign(payload,process.env.SECRETA, {
         expiresIn: 36000
      }, (error, token) => {
         if(error) throw error;

         // confirmation message
         res.json({ token });

      });

      
   } catch (error) {
      console.log(error);
      res.status(400).send("Hubo un error");
   }
};
