const Tarea = require("../models/Tareas");
const Proyecto = require("../models/Proyecto");
const { validationResult } = require("express-validator");

// crea una nueva tarea
exports.crearTarea = async (req, res) => {

   // check errors
   const errors = validationResult(req);
   if (!errors.isEmpty()) {
      return res.status(400).json({ errores: errors.array() });
   }

   try {
      // extraer el proyecto
      const { proyecto } = req.body;

      const existProyecto = await Proyecto.findById(proyecto);
      if (!existProyecto) {
         return res.status(404).json({ msg: "Proyecto no encontrado" });
      }

      // revisar si el proyecto actual pertenece al usuario autenticado
      if (existProyecto.propietario.toString() !== req.usuario.id) {
         return res.status(401).json({ msg: "No autorizado " });
      }

      // creamos la tarea
      const tarea = new Tarea(req.body);
      await tarea.save();
      res.json({ tarea });

   } catch (error) {
      console.log(error);
      res.status(500).send("Hubo un error");
   }
};

// obtiene las tareas de un proyecto
exports.obtenerTareas = async (req, res) => {

   try {
      // extraer el proyecto
      const { proyecto } = req.query;

      const existProyecto = await Proyecto.findById(proyecto);
      if (!existProyecto) {
         return res.status(404).json({ msg: "Proyecto no encontrado" });
      }

      // revisar si el proyecto actual pertenece al usuario autenticado
      if (existProyecto.propietario.toString() !== req.usuario.id) {
         return res.status(401).json({ msg: "No autorizado " });
      }

      // obtener tareas por proyecto
      const tareas = await Tarea.find({ proyecto });
      res.json({ tareas });

   } catch (error) {
      console.log(error);
      res.status(500).send("Hubo un eror");
   }
};

// actualiar tarea
exports.actualizarTarea = async(req, res) => {
   try {
      // extraer el proyecto
      const { proyecto, nombre, estado } = req.body;

      // revisar si la tarea existe 
      let tarea = await Tarea.findById(req.params.id);
      if(!tarea) {
         return res.status(404).json({ msg: "No existe esa tarea " });
      }

      // revisar si el proyecto actual pertenece al usuario autenticado
      const existProyecto = await Proyecto.findById(proyecto);
      if (existProyecto.propietario.toString() !== req.usuario.id) {
         return res.status(401).json({ msg: "No autorizado " });
      }

      // crear un objeto con la nueva información
      const nuevaTarea = {};
      nuevaTarea.nombre = nombre;
      nuevaTarea.estado = estado;

      // guardar la tarea
      tarea = await Tarea.findOneAndUpdate({_id: req.params.id}, nuevaTarea, {new: true})

      res.json({tarea})
   } catch (error) {
      console.log(error);
      res.status(500).send("Hubo un eror");  
   }
}

// elimina una tarea
exports.eliminarTarea = async(req, res) => {
   // extraer el proyecto
   const { proyecto } = req.query;

   // revisar si la tarea existe 
   let tarea = await Tarea.findById(req.params.id);
   if(!tarea) {
      return res.status(404).json({ msg: "No existe esa tarea " });
   }

   // revisar si el proyecto actual pertenece al usuario autenticado
   const existProyecto = await Proyecto.findById(proyecto);
   if (existProyecto.propietario.toString() !== req.usuario.id) {
      return res.status(401).json({ msg: "No autorizado " });
   }

   // eliminar
   await Tarea.findOneAndRemove({_id: req.params.id});
   res.json({msg: "Tarea eliminada."});
}
