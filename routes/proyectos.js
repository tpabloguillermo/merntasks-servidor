const express = require("express");
const router = express.Router();
const proyectoController = require("../controllers/proyectoController");
const auth = require("../middleware/auth");
const {check} = require("express-validator");


// crea proyectos
// api/proyectos

/* 
   Se construye un middleware para  el token que viene en la request y verificar q sea válido,
   si es válido entonces se usa el token + palabra secreta (archivo .env) con la funcion de
   jwt verify(token,palabra secreta) para decodificar el token y saber su contenido, que este
   va a ser el payload que se envié cuando se está genere el token para el usuario
   y despues pasa a el siguiente middleware con next() 
*/
router.post("/",
   auth,
   [
      check("nombre", "El nombre del proyecto es obligatorio").notEmpty()
   ],
   proyectoController.crearProyecto
);

router.get("/",
   auth,
   proyectoController.obtenerProyectos
);

router.put("/:id",
   auth,
   [
      check("nombre", "El nombre del proyecto es obligatorio").notEmpty()
   ],
   proyectoController.actualizarProyecto
)

// elminar un proyecto
router.delete("/:id",
   auth,
   proyectoController.eliminarProyecto
)

module.exports = router;